FROM alpine
COPY build/libs/practice03-0.0.1-SNAPSHOT.jar /app/practice03-0.0.1-SNAPSHOT.jar
RUN apk add --no-cache openjdk17
WORKDIR "/app"
ENTRYPOINT ["java"]
CMD ["-jar", "practice03-0.0.1-SNAPSHOT.jar"]