package ady.az.practice03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Practice03App {

    public static void main(String[] args) {
        SpringApplication.run(Practice03App.class, args);

    }

}
