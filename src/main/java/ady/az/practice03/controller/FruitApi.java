package ady.az.practice03.controller;


import ady.az.practice03.dto.FruitRequestDto;
import ady.az.practice03.dto.FruitResponseDto;
import ady.az.practice03.services.FruitService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/fruits")
public class FruitApi {

    private final FruitService fruitService;

    public FruitApi(FruitService fruitService) {
        this.fruitService = fruitService;
    }


    @GetMapping
    public List<FruitResponseDto> getFruitList(@RequestParam(value = "from", required = false, defaultValue = "0") Long from,
                                               @RequestParam(value = "to", required = false, defaultValue = "10") Long to) {

        return fruitService.getFruitList(from, to);
    }

    @GetMapping("/{id}")
    public FruitResponseDto getFruit(@PathVariable Long id) {
        return fruitService.getFruit(id);
    }

    @PostMapping()
    public FruitResponseDto createFruit(@Validated @RequestBody FruitRequestDto fruit) {

        return fruitService.createFruit(fruit);
    }


    @PutMapping("/{id}")
    public FruitResponseDto updateFruit(@PathVariable Long id, @Validated @RequestBody FruitRequestDto fruit) {
        return fruitService.updateFruit(id, fruit);

    }


    @DeleteMapping("/{id}")
    public String deleteFruit(@PathVariable Long id) {
        fruitService.deleteFruit(id);
        return "success deleted " + id;
    }
}


// generate seeder
/*

    private final List<FruitDto> fruits = new ArrayList<>();

    @PostConstruct
    public List<FruitDto> getFruits() {
        for (int i = 0; i < 25; i++) {
            fruits.add(i, new FruitDto(i+" apple ",10+i, 2.3));
        }
        return fruits;
    }
 */