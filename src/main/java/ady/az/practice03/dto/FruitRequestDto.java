package ady.az.practice03.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FruitRequestDto {

    @NotBlank
    String name;
    @NotNull
    int amount;
    @NotNull
    Double price;
}
