package ady.az.practice03.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FruitResponseDto {


    private Long id;
    private String name;
    private int amount;
    private Double price;
}
