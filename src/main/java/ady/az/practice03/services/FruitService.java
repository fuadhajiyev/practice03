package ady.az.practice03.services;

import ady.az.practice03.dto.FruitRequestDto;
import ady.az.practice03.dto.FruitResponseDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


public interface FruitService {


    List<FruitResponseDto> getFruitList(@RequestParam(value = "from", required = false, defaultValue = "0") Long from,
                                        @RequestParam(value = "to", required = false, defaultValue = "10") Long to);


    FruitResponseDto getFruit(@PathVariable Long id);


    FruitResponseDto createFruit(@Validated @RequestBody FruitRequestDto fruit);


    FruitResponseDto updateFruit(@PathVariable Long id, @Validated @RequestBody FruitRequestDto fruit);


    void deleteFruit(@PathVariable Long id);
}
