package ady.az.practice03.services;

import ady.az.practice03.domain.Fruit;
import ady.az.practice03.dto.FruitRequestDto;
import ady.az.practice03.dto.FruitResponseDto;
import ady.az.practice03.repository.FruitRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FruitServiceImpl implements FruitService {

    private final FruitRepository fruitRepository;
    private final ModelMapper modelMapper;

    public FruitServiceImpl(FruitRepository fruitRepository, ModelMapper modelMapper) {
        this.fruitRepository = fruitRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<FruitResponseDto> getFruitList(Long from, Long to) {

        return fruitRepository.findAll()
                .stream()
                .map(fruit -> modelMapper.map(fruit, FruitResponseDto.class)).toList();
    }

    @Override
    public FruitResponseDto getFruit(Long id) {

        return fruitRepository.findById(id)
                .map(fruit -> modelMapper.map(fruit, FruitResponseDto.class))
                .orElseThrow(() -> new RuntimeException());

    }

    @Override
    public FruitResponseDto createFruit(FruitRequestDto fruitDto) {

        Fruit fruit = modelMapper.map(fruitDto, Fruit.class);
        return modelMapper.map(fruitRepository.save(fruit), FruitResponseDto.class);

    }


    @Override
    public FruitResponseDto updateFruit(Long id, FruitRequestDto fruitDto) {

        final Fruit responseFruit = modelMapper.map(fruitDto, Fruit.class);
        responseFruit.setId(id);
        fruitRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Fruit with id " + id + " not found"));

        return modelMapper.map(fruitRepository.save(responseFruit), FruitResponseDto.class);

    }

    @Override
    public void deleteFruit(Long id) {

        fruitRepository.deleteById(id);
    }
}
